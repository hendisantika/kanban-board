# Kanban Board

### Things todo list:

1. Clone this repository: `git clone https://gitlab.com/hendisantika/kanban-board.git`
2. Go inside the folder: `cd kanban-board`
3. Run Backend & Frontend App with docker-compose: `docker-compose up -d`
4. Open your favorite browser: http://localhost:4200

### Images Screen shot

Create New Kanban Board

![Create New Kanban Board](img/create.png "Create New Kanban Board")

Create New Task in Kanban Board

![Create New Task in Kanban Board](img/testing.png "Create New Task in Kanban Board")

![Create New Task in Kanban Board](img/task1.png "Create New Task in Kanban Board")

![Create New Task in Kanban Board](img/task2.png "Create New Task in Kanban Board")

![Create New Task in Kanban Board](img/task3.png "Create New Task in Kanban Board")

![Create New Task in Kanban Board](img/task4.png "Create New Task in Kanban Board")

Swagger UI

You can open swagger UI in this link http://localhost:8080/api/swagger-ui.html

![Swaager UI](img/swagger1.png "Swagger UI")

![Swaager UI](img/swagger2.png "Swagger UI")

![Swaager UI](img/swagger3.png "Swagger UI")

![Swaager UI](img/swagger4.png "Swagger UI")

![Swaager UI](img/swagger5.png "Swagger UI")

![Swaager UI](img/swagger6.png "Swagger UI")

![Swaager UI](img/swagger7.png "Swagger UI")