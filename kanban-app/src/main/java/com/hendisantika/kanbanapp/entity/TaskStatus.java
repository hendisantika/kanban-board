package com.hendisantika.kanbanapp.entity;

import io.swagger.annotations.ApiModel;

/**
 * Created by IntelliJ IDEA.
 * Project : kanban-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/03/21
 * Time: 07.07
 */
@ApiModel
public enum TaskStatus {
    TODO, INPROGRESS, DONE
}