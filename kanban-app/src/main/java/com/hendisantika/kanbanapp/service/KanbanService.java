package com.hendisantika.kanbanapp.service;

import com.hendisantika.kanbanapp.dto.KanbanDTO;
import com.hendisantika.kanbanapp.dto.TaskDTO;
import com.hendisantika.kanbanapp.entity.Kanban;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : kanban-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/03/21
 * Time: 07.11
 */
public interface KanbanService {
    List<Kanban> getAllKanbanBoards();

    Optional<Kanban> getKanbanById(Long id);

    Optional<Kanban> getKanbanByTitle(String title);

    Kanban saveNewKanban(KanbanDTO kanbanDTO);

    Kanban updateKanban(Kanban oldKanban, KanbanDTO newKanbanDTO);

    void deleteKanban(Kanban kanban);

    Kanban addNewTaskToKanban(Long kanbanId, TaskDTO taskDTO);
}
